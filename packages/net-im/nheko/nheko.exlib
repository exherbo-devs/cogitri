# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix=https://nheko.im user=nheko-reborn tag=v${PV} new_download_scheme=true ]
require cmake
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_compile src_install pkg_postinst pkg_postrm

SUMMARY="Desktop client for the Matrix protocol"

LICENCES="GPL-3"
SLOT="0"

MYOPTIONS="
    doc
    screencast [[ description = [ Support for sharing your screen using xdg-desktop-portal ] ]]
    voip [[ description = [ Support for Voice over IP ] ]]
    X

    screencast [[ requires = voip ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        app-text/cmark:=[>=0.29.0]
        dev-db/lmdb
        dev-libs/blurhash
        dev-libs/cpp-httplib
        dev-libs/fmt[>=8.0.0]
        dev-libs/json[>=3.2.0]
        dev-libs/kdsingleapplication
        dev-libs/libevent:=
        dev-libs/lmdbxx
        dev-libs/mtxclient[>=0.10.0]
        dev-libs/olm[>=3.2.7]
        dev-libs/openssl:=[>=1.1.0]
        dev-libs/re2:=
        dev-libs/spdlog[>=1.0.0]
        net-misc/coeurl[>=0.3.0]
        net-misc/curl
        sys-auth/qtkeychain[providers:qt6]
        x11-libs/qtbase:6[>=6.5]
        x11-libs/qtdeclarative:6[>=6.5]
        x11-libs/qtmultimedia:6[>=6.5]
        x11-libs/qtsvg:6[>=6.5]
        x11-libs/qttools:6[>=6.5]
        X? (
            x11-libs/libxcb
            x11-utils/xcb-util-wm
        )
        voip? (
            media-plugins/gst-plugins-bad:1.0[gstreamer_plugins:webrtc]
            media-plugins/gst-plugins-base:1.0[gstreamer_plugins:opengl] [[ note = [ sdp ] ]]
        )
    run:
        screencast? ( sys-apps/xdg-desktop-portal )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DASAN:BOOL=FALSE
    -DMAN:BOOL=TRUE
    # Hunter package manager
    -DHUNTER_ENABLED:BOOL=FALSE
    -DUSE_BUNDLED_BLURHASH:BOOL=FALSE
    -DUSE_BUNDLED_CMARK:BOOL=FALSE
    -DUSE_BUNDLED_COEURL:BOOL=FALSE
    -DUSE_BUNDLED_CPPHTTPLIB:BOOL=FALSE
    -DUSE_BUNDLED_FMT:BOOL=FALSE
    -DUSE_BUNDLED_JSON:BOOL=FALSE
    -DUSE_BUNDLED_KDSINGLEAPPLICATION:BOOL=FALSE
    -DUSE_BUNDLED_LIBCURL:BOOL=FALSE
    -DUSE_BUNDLED_LIBEVENT:BOOL=FALSE
    -DUSE_BUNDLED_LMDB:BOOL=FALSE
    -DUSE_BUNDLED_LMDBXX:BOOL=FALSE
    -DUSE_BUNDLED_MTXCLIENT:BOOL=FALSE
    -DUSE_BUNDLED_OLM:BOOL=FALSE
    -DUSE_BUNDLED_OPENSSL:BOOL=FALSE
    -DUSE_BUNDLED_QTKEYCHAIN:BOOL=FALSE
    -DUSE_BUNDLED_RE2:BOOL=FALSE
    -DUSE_BUNDLED_SPDLOG:BOOL=FALSE
    -DQML_DEBUGGING:BOOL=FALSE
)

CMAKE_SOURCE="${WORKBASE}/${PN}-v${PV}"

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'screencast SCREENSHARE_XDP'
    VOIP
    'X X11'
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    'doc DOCS'
)

nheko_src_compile() {
    cmake_src_compile

    option doc && ecmake_build --target docs
}

nheko_src_install() {
    cmake_src_install

    if option doc ; then
        edo pushd docs
        dodoc -r html
        edo popd
    fi
}

nheko_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

nheko_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

